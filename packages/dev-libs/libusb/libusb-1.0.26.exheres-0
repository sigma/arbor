# Copyright 2013-2015 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ release=v${PV} suffix=tar.bz2 ]

SUMMARY="library that provides generic access to USB devices"
HOMEPAGE+=" http://${PN}.info"

LICENCES="LGPL-2.1"
SLOT="1"
PLATFORMS="~amd64 ~arm ~armv7 ~armv8 ~x86"
MYOPTIONS="
    syslog [[ description = [ output logging messages to syslog ] ]]
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        sys-kernel/linux-headers[>=2.6.27]
    build+run:
        providers:eudev? ( sys-apps/eudev )
        providers:systemd? ( sys-apps/systemd )
        !dev-libs/libusbx:1 [[
            description = [ dev-libs/libusbx won and was renamed back to libusb ]
            resolution = uninstall-blocked-after
        ]]
    test:
        dev-libs/umockdev[>=0.16.0]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-udev
    --disable-static
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    "syslog system-log"
)

DEFAULT_SRC_CONFIGURE_TESTS=( '--enable-tests-build --disable-tests-build' )

DEFAULT_SRC_COMPILE_PARAMS=( -j1 )

src_test() {
    default

    edo tests/stress
}

