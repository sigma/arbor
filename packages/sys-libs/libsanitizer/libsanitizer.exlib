# Copyright 2018 Tom Briden <tom@decompile.me.uk>
# Based on the libatomic exheres which is
#   Copyright 2015 Saleem Abdulrasool <compnerd@compnerd.org>
#   Distributed under the terms of the GNU General Public License v2

require gcc.gnu.org
require toolchain-runtime-libraries

SUMMARY="GCC Sanitizer Libraries"

LICENCES="GPL-2"
SLOT="$(ever major)"

DEPENDENCIES="
    build:
        sys-libs/libstdc++:${SLOT}
"

MYOPTIONS=""

if [[ ${PV} == *_pre* ]] ; then
    ECONF_SOURCE="${WORKBASE}/gcc-$(ever major)-${PV##*_pre}/${PN}"
    WORK="${WORKBASE}/gcc-$(ever major)-${PV##*_pre}/build/$(exhost --target)/${PN}"
else
    ECONF_SOURCE="${WORKBASE}/gcc-${PV/_p?(re)/-}/${PN}"
    WORK="${WORKBASE}/gcc-${PV/_p?(re)/-}/build/$(exhost --target)/${PN}"
fi

DEFAULT_SRC_CONFIGURE_PARAMS=( --disable-multilib )

libsanitizer_src_unpack() {
    default
    edo mkdir -p "${WORK}"
}

libsanitizer_src_prepare() {
    #Force to use the already installed libstdc++.la for this target
    edo sed -i 's|\\\$(top_builddir)/../libstdc++-v3/src|/usr/'"$(exhost --target)"'/lib|' "${ECONF_SOURCE}/configure"
    edo cd "${ECONF_SOURCE}/.."
    default
}

libsanitizer_src_configure() {
    CC=$(exhost --tool-prefix)gcc-${SLOT}       \
    CPP=$(exhost --tool-prefix)gcc-cpp-${SLOT}  \
    CXX=$(exhost --tool-prefix)g++-${SLOT}      \
    default
}

libsanitizer_src_install() {
    default

    symlink_dynamic_libs lib{a,l,t,ub}san
    slot_dynamic_libs lib{a,l,t,ub}san
    slot_other_libs lib{a,l,t,ub}san.{a,la}
    slot_other_libs lib{a,l,t,ub}san_preinit.o
    slot_other_libs ${PN}.spec

}

export_exlib_phases src_unpack src_prepare src_configure src_install

