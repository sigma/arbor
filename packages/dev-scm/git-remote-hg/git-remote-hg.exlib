# Copyright 2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=mnauw tag=v${PV} ]
# Not really py2 only, but let's get rid of it where we easily can
require python [ blacklist=2 multibuild=false ]

export_exlib_phases src_compile src_install

SUMMARY="Transparent bidirectional bridge between Git and Mercurial for Git"
DESCRIPTION="
git-remote-hg is the semi-official Mercurial bridge from Git project, once
installed, it allows you to clone, fetch and push to and from Mercurial
repositories as if they were Git ones, e. g.:
git clone hg::http://selenic.com/repo/hello
"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    build:
        app-doc/asciidoc
    run:
        dev-scm/mercurial[python_abis:*(-)?]
         !dev-scm/git-remote-helpers[mercurial] [[
            description = [ A maintained fork of the one in git-remote-helpers ]
            resolution = uninstall-blocked-after
        ]]
"

git-remote-hg_src_compile() {
    emake PYTHON="python$(python_get_abi)"
}

git-remote-hg_src_install() {
    emake DESTDIR="${IMAGE}" prefix=/usr/$(exhost --target) install
    emake DESTDIR="${IMAGE}" prefix=/usr install-doc
}

