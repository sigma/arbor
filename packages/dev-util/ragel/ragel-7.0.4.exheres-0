# Copyright 2009 Mike Kelly <pioto@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

# ragel-Don-t-hard-code-gcc.patch
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 1.15 ] ]

SUMMARY="Ragel State Machine Compiler"
DESCRIPTION="
Ragel compiles executable finite state machines from regular languages.
Ragel targets C, C++, Objective-C, D, Java and Ruby. Ragel state
machines can not only recognize byte sequences as regular expression
machines do, but can also execute code at arbitrary points in the
recognition of a regular language. Code embedding is done using inline
operators that do not disrupt the regular language syntax.
"
HOMEPAGE="https://www.colm.net/open-source/${PN}"
DOWNLOADS="https://www.colm.net/files/${PN}/${PNV}.tar.gz"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-lang/colm[>=0.14.7]
"

DEFAULT_SRC_PREPARE_PATCHES+=(
    "${FILES}"/${PN}-link-colm-properly.patch
)

DEFAULT_SRC_CONFIGURE_PARAMS=(
    # needs fig2dev and pdflatex
    --disable-manual
    --with-colm=/usr/$(exhost --target)
)

src_prepare() {
    edo sed \
        -e "s|COLM_SHARE=.*|COLM_SHARE=\"/usr/share/colm\"|" \
        -i configure.ac

    autotools_src_prepare
}

