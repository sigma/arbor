# Copyright 2015-2016 Kylie McClain <somasis@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=Orc tag=v${PV} ]

SUMMARY="An implementation in C of John Gruber's Markdown"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64 ~armv8"
MYOPTIONS="
    cxx [[ description = [ Build C++ bindings ] ]]
    debug
"

DEPENDENCIES=""

# Fix -lmarkdown linking error by following make build order
DEFAULT_SRC_COMPILE_PARAMS=( -j1 )

src_prepare() {
    default

    # TODO: fix upstream
    for shim in ar cpp ranlib; do
        edo ln -s /usr/$(exhost --target)/bin/$(exhost --tool-prefix)${shim} ${shim}
    done
}

src_prepare() {
    edo sed \
        -e '/test "\$LDCONFIG"/d' \
        -e 's/_strip="-s"/unset _strip/' \
        -i configure.inc
}

src_configure() {
    local myconf=(
        --prefix=/usr/$(exhost --target)
        --mandir=/usr/share/man
        --pkg-config
        --shared
        --enable-all-features
        --with-latex
        $(optionq cxx && echo --cxx-binding)
        $(optionq debug && echo --enable-amalloc)
    )

    edo ./configure.sh "${myconf[@]}"
}

src_install() {
    emake install.everything DESTDIR="${IMAGE}"
}

