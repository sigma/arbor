Title: ctags alternative has changed to universal-ctags
Author: Timo Gurr <tgurr@exherbo.org>
Content-Type: text/plain
Posted: 2021-08-17
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: dev-util/ctags

The ctags alternative has switched from ctags to universal-ctags and
ctags has been removed. A simple cave resolve command should complete
the switch:

cave resolve -c world --permit-uninstall dev-util/ctags
